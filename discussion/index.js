console.log("Hi");

console.log(document);
// Result : document html code

// const txtFirstName = document.querySelector("#txt-first-name");

// console.log(txtFirstName);
//result :input field/tag

/*
	alternative ways: 
	document.getElementById("txt-first-name");
	document.getElementByClassName("text-class");
	document.getElementByTagName("th1");
*/


//target the full name

// let spanFullName = document.querySelector("#span-full-name");
// console.log(spanFullName)

// //Event Listeners
// //event can have shorthand of (e)
// txtFirstName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
// })

// txtFirstName.addEventListener('keyup',(event) => {
// 	console.log(event);
// 	console.log(event.target);
// 	console.log(event.target.value);
//})

//stretch
// const keyCodeEvent = (e) => {
// 	let kc = e.keyCode;
// 	if (kc === 65 ){
// 		e.target.value = null;
// 		alert('Someone clicked a');
// 	}
// }

//txtFirstName.addEventListener('keyup', keyCodeEvent);



// Activity Solution No. 2

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// const firstName = (event) => {
// 	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
// }


// const lastName = (event) => {
// 	spanFullName.innerHTML =  txtFirstName.value + " " +txtLastName.value;
// }

// txtFirstName.addEventListener('keyup',firstName);
// txtLastName.addEventListener('keyup',lastName);

const fullName = (e) => {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}

txtFirstName.addEventListener('keyup',fullName);
txtLastName.addEventListener('keyup',fullName);